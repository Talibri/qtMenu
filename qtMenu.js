// ==UserScript==
// @name         qtMenu
// @namespace    https://talibri.com/
// @version      0.8.7
// @description  Quick Talibri Menu
// @author       finally
// @match        *://*.talibri.com/*
// @grant        none
// ==/UserScript==

(function() {
  'use strict';

  var houses = [];
  var cities = ['Gild', 'Lowenheim', 'Emelglad', 'Cauruin', 'Necropolis'];
  var professions = ['Alchemy', 'Blacksmithing', 'Construction', 'Cooking', 'Enchanting', 'Tailoring', 'Weaponsmithing', 'Woodworking'];
  var professionsCityId = [
    [15, 12, 16, 2, 1, 7, 13, 14],
    [6, 3, 8, 9, 11, 10, 4, 5],
    [23, 20, 24, 17, 19, 18, 21, 22],
    [31, 28, 32, 25, 27, 26, 29, 30],
    [39, 36, 40, 33, 35, 34, 37, 38]
  ];

  var gatheringGrounds = ['Erets Forest', 'Mikhail Mountain', 'Enmuyama Mountain', 'Dragonmount', 'Glaurduin', 'Veran Plains', 'Andaelin Lake', 'Dorran Mines'];
  var gatheringGroundIds = [1, 4, 5, 6, 7, 10, 12, 13];

  var combatZones = ['Calin Beach', 'Fields of Paelin', 'Spdier Cave', 'Ruins of Morgaroth', 'The Tower of Ice', 'Abyssal Rift', 'Fulgur Deep', 'Lowenheim Outskirts', 'Ariz Desert', 'Dead Scar - South'];
  var combatZoneIds = [3, 1, 2, 4, 6, 5, 7, 8, 9, 10];

  var additionTypes = [
    ['Kitchen', 'Pond', 'Grove', 'Quarry', 'Greenhouse', 'Forge', 'Alchemist Lab', 'Carpenters', 'Workshop', 'Weaving'],
    ['Fridge', 'Dry Dock', 'Lumber Yard', 'Ore Storage', 'Trimming Chamber', 'Materials Room', 'Herb Storage', 'Construction Yard', 'Planing', 'Mannequin Storage'],
    ['Pantry', 'Bait Storage', 'Woodsman', 'Prospectors', 'Drying', 'Tempering', 'Beaker', 'Tool Shed', 'Polish', 'Fitting'],
    ['Training Yard']
  ];

  window.addStyle = function(css){
    var style = document.createElement('style');
    style.innerHTML = css;

    document.head.appendChild(style);
  };

  window.uuidv4 = function(){
    return "10000000-1000-4000-8000-100000000000".replace(/[018]/g, function(c){
      return (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16);
    });
  };

  window.qtRequest = function(m,u,f,e,o){
    var req = new XMLHttpRequest();
    
    if(f){
      req.onload = function(){f.call(this, o);};
    }

    if(e){
      req.onerror = function(){e.call(this, o);};
    }    
    
    req.open(m, u);
    req.setRequestHeader('X-CSRF-Token', document.querySelector('meta[name=csrf-token]').getAttribute('content'));
    req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    req.send();
  };

  window.qtLoad = function(){
    var profile = document.querySelector('a[href^="/user/"][href$="/profile"]').href;
    window.qtRequest("GET", "/user/" + profile.substring(profile.indexOf("/user/")+6, profile.lastIndexOf("/")) + "/home", function(e){
      if(this.status != 200){
        setTimeout(window.qtLoad, 1000);
        return;
      }

      var r = /\<strong\>Home\sSpace\<\\\/strong\>\:\s*(.*?)\\n(?:.*?)\<strong\>City\<\\\/strong\>\:\s*(.*?)\\n/gm;
      var m = r.exec(this.responseText);

      if(!m || m.length <= 0){
        setTimeout(window.qtLoad, 1000);
        return;
      }

      localStorage['qt-house'] = m[1];
      localStorage['qt-city'] = cities.indexOf(m[2]);
      
      r = /\\n\s+([\w\s()]+)\s+\(Level\s(\d+)\)(?:.*?)open_addition\?addition_id\=(\d+)/gm;
      var i = 0;
      while ((m = r.exec(this.responseText)) != null){
        if(localStorage['qt-addition-id'+i] != m[3]){
          localStorage.removeItem('qt-addition-name'+i);
          localStorage.removeItem('qt-addition-level'+i);
          localStorage.removeItem('qt-addition-id'+i);
          localStorage.removeItem('qt-addition-type'+i);
          localStorage.removeItem('qt-addition-goal'+i);
          localStorage.removeItem('qt-addition-used'+i);
        }

        localStorage['qt-addition-name'+i] = m[1];
        localStorage['qt-addition-level'+i] = m[2];
        localStorage['qt-addition-id'+i] = m[3];
        localStorage['qt-addition-type'+i] = -1;
        for(var j = 0;j < additionTypes.length;j++){
          for(var k = 0;k < additionTypes[j].length;k++){
            if(m[1].indexOf(additionTypes[j][k]) != -1){
              localStorage['qt-addition-type'+i] = j;
              break;
            }
          }
          if(localStorage['qt-addition-type'+i] != -1){
            break;
          }
        }
        
        i++;
      }

      for(;i < 100;i++){
        localStorage.removeItem('qt-addition-name'+i);
        localStorage.removeItem('qt-addition-level'+i);
        localStorage.removeItem('qt-addition-id'+i);
        localStorage.removeItem('qt-addition-type'+i);
        localStorage.removeItem('qt-addition-goal'+i);
        localStorage.removeItem('qt-addition-used'+i);
      }

      window.qtInit();
    });
  };

  window.qtNavMenu = function(){
    var nav = document.querySelectorAll('.navbar-nav')[0];
    var old = nav.querySelector('li');
    old.style.display = 'none';

    window.addStyle(
      '.qt-nav:hover, .qt-menu-item:hover {' +
      '  background: #e7e7e7;' +
      '}' +

      '.qt-nav {' +
      '  text-align: center;' +
      '}' +

      '.qt-nav:hover > .qt-menu {' +
      '  display: block;' +
      '}' +

      '.qt-menu-item:hover > .qt-submenu {' +
      '  display: block;' +
      '}' +

      '.qt-menu, .qt-submenu {' +
      '  position: absolute;' +
      '  display: none;' +
      '}' +

      '.qt-menu {' +
      '  left: 0;' +
      '  text-align: center;' +
      '  width: 100%;' +
      '}' +

      '.qt-menu-item {' +
      '  position: relative;' +
      '}' +

      '.qt-menu-item > a {' +
      '  display:inline-block;' +
      '  width:100%;' +
      '  height:100%;' +
      '  padding: 5px;' +
      '}' +

      '.qt-submenu {' +
      '  left: 100%;' +
      '  position: absolute;' +
      '  padding: 0px;' +
      '  top: 0px;' +
      '}' +

      '.qt-menu ul, .qt-submenu ul {' +
      '  padding: 0;' +
      '}' +

      '.qt-menu li, .qt-submenu li {' +
      '  white-space: nowrap;' +
      '  list-style-type: none;' +
      '  background: #f8f8f8;' +
      '}'
    );

    if(localStorage['qt-hide-nav'] == 'true'){
      old.style.display = 'block';
      return;
    }

    var html = 
    '<a href="#"><i class="fa fa-globe"></i> Locations <span class="caret"></span></a>' +
    '<div class="qt-menu">' +
    '  <ul>' +
    '    <li class="qt-menu-item">' +
    '      <a href="/cities">Cities</a>' +
    '      <div class="qt-submenu">' +
    '        <ul>' +
    '          <li class="qt-menu-item"><a href="/trade/1">Market</a></li>';

    for(var i = 0;i < cities.length; i++){
      html += '<li class="qt-menu-item"><a href="/cities/' + cities[i] + '">' + cities[i] + '</a>';

      html +=
      '      <div class="qt-submenu">' +
      '        <ul>';
      for(var j = 0;j < professions.length; j++){
        html += '<li class="qt-menu-item"><a href="/crafting/' + professionsCityId[i][j] + '?city_id=' + (i+1) + '">' + professions[j] + '</a></li>';
      }
      html +=
      '        </ul>' +
      '      </div>' +
      '    </li>';
    }

    html += 
    '        </ul>' +
    '      </div>' +
    '    </li>' +
    '    <li class="qt-menu-item">' +
    '      <a href="#">Professions</a>' +
    '      <div class="qt-submenu">' +
    '        <ul>';

    var city = ~~localStorage['qt-city'] || 1;
    for(i = 0;i < professions.length; i++){
      html += '<li class="qt-menu-item"><a href="/crafting/' + professionsCityId[city][i] + '?city_id=' + (city+1) + '">' + professions[i] + '</a></li>';
    }

    html += 
    '        </ul>' +
    '      </div>' +
    '    </li>' +
    '    <li class="qt-menu-item">' +
    '      <a href="/locations/gathering_grounds">Gathering</a>' +
    '      <div class="qt-submenu">' +
    '        <ul>';

    for(i = 0;i < gatheringGrounds.length;i++){
      html += '<li class="qt-menu-item"><a href="https://talibri.com/locations/' + gatheringGroundIds[i] + '/show">' + gatheringGrounds[i] + '</a></li>';
    }

    html += 
    '        </ul>' +
    '      </div>' +
    '    </li>' +
    '    <li class="qt-menu-item">' +
    '      <a href="/combat_zones">Combat</a>' +
    '      <div class="qt-submenu">' +
    '        <ul>';

    for(i = 0;i < combatZones.length;i++){
      html += '<li class="qt-menu-item"><a href="https://talibri.com/combat_zones/' + combatZoneIds + '/adventure">' + combatZones[i] + '</a></li>';
    }

    html += 
    '        </ul>' +
    '      </div>' +
    '    </li>' +
    '  </ul>' +
    '</div>';

    var li = document.createElement('li');
    li.className = 'qt qt-nav';
    li.innerHTML = html;

    nav.insertBefore(li, old);
  };

  window.qtMenu = function(){
    window.addStyle(
      'table.qt {' +
      '  width: 100%;' +
      '  font-size: 10px;' +
      '  overflow: hidden;' +
      '  white-space: nowrap;' +
      '  table-layout: fixed;' +
      '}' +

      'table.qt td {' +
      '  width: 11.11%;' +
      '  overflow: hidden;' +
      '  text-overflow: ellipsis;' +
      '}' +

      'table.qt hr {' +
      '  border-color: black;' +
      '  margin: 5px 0;' +
      '}'
    );

    if(localStorage['qt-hide-top'] == 'true') return;

    var main = document.querySelector('.main-page');
    if(!main) return;

    var menu = document.createElement('div');
    menu.className = 'qt breadcrumb';

    var html = 
    '  <table class="qt">' +
    '    <tr>';

    var i = 0, n = 0;

    if(localStorage['qt-hide-market'] != 'true'){
      html += '<td><a href="/trade/1">Market</a></td>';
      n++;
    }

    for(;i < cities.length; i++){
      if(localStorage['qt-hide-city'+i] == 'true') continue;

      html += '<td><a href="/cities/' + cities[i] + '">' + cities[i] + '</a></td>';

      n++;
    }

    if(n > 0) html += '<tr><td colspan="9"><hr/></td></tr>';
    
    html += '</tr>';

    html += '<tr>';
    var city = ~~localStorage['qt-city'] || 1;
    for(i = 0, n = 0;i < professions.length; i++){
      if(localStorage['qt-hide-profession'+i] == 'true') continue;

      html += '<td><a href="/crafting/' + professionsCityId[city][i] + '?city_id=' + (city+1) + '">' + professions[i] + '</a></td>';

      n++;
    }
    html += '</tr>';

    if(n > 0) html += '<tr><td colspan="9"><hr/></td></tr>';

    html += '</tr>';

    if(localStorage['qt-hide-gathering'] != 'true') html += '<td><a href="/locations/gathering_grounds">Gathering</a></td>';

    for(i = 0, n = 0;i < gatheringGrounds.length;i++){
      if(localStorage['qt-hide-gathering'+i] == 'true') continue;

      html += '<td><a href="https://talibri.com/locations/' + gatheringGroundIds[i] + '/show">' + gatheringGrounds[i] + '</a></td>';

      n++;
    }

    html += '</tr>';

    if(n > 0) html += '<tr><td colspan="9"><hr/></td></tr>';
    
    html += '</tr>';

    if(localStorage['qt-hide-combat'] != 'true') html += '<td><a href="/combat_zones">Combat</a></td>';

    for(i = 0, n = 0;i < combatZones.length;i++){
      if(localStorage['qt-hide-combat'+i] == 'true') continue;

      if(n == 7) html += '<tr/><tr><td></td>';
      html += '<td><a href="https://talibri.com/combat_zones/' + combatZoneIds[i] + '/adventure">' + combatZones[i] + '</a></td>';

      n++;
    }

    html += 
    '    </tr>' +
    '  </table>';

    menu.innerHTML = html;

    main.prepend(menu);
  };

  window.qtSettingsMenu = function(){
    window.addStyle(
      '@keyframes qt-notify {' +
      '  0% {' +
      '    text-shadow: 0px 0px 5px rgba(255,0,0,0);' +
      '  }' +
      '  50% {' +
      '    text-shadow: 0px 0px 5px rgba(255,0,0,1);' +
      '  }' +
      '  100% {' +
      '    text-shadow: 0px 0px 5px rgba(255,0,0,0);' +
      '  }' +
      '}' +

      '.qt-notify {' +
      '  animation: qt-notify 2s infinite;' +
      '}'
    );

    var nav = document.querySelectorAll('.navbar-right')[0];
    var profile = nav.querySelector('li');

    var li = document.createElement('li');
    li.className = 'qt qt-nav';
    li.style.width = '100px';
    li.innerHTML = '<a href="#">qt <span class="caret"></span></a>';

    var div = document.createElement('div');
    div.className = 'qt-menu';

    var ul = document.createElement('ul');

    var liAdditions = document.createElement('li');
    liAdditions.className = 'qt-menu-item';
    liAdditions.innerHTML = '<a href="#">qtAdditions</a>';

    liAdditions.onclick = function(e){
      e.preventDefault();

      var calculator = document.querySelector('.qt-additions');
      if(calculator.style.display == 'block'){
        calculator.style.display = 'none';
      } else {
        calculator.style.display = 'block';
      }
    };

    ul.appendChild(liAdditions);

    var liCalculator = document.createElement('li');
    liCalculator.className = 'qt-menu-item';
    liCalculator.innerHTML = '<a href="#">qtCalculator</a>';

    liCalculator.onclick = function(e){
      e.preventDefault();

      var calculator = document.querySelector('.qt-calculator');
      if(calculator.style.display == 'block'){
        calculator.style.display = 'none';
      } else {
        calculator.style.display = 'block';
      }
    };

    ul.appendChild(liCalculator);

    var liSettings = document.createElement('li');
    liSettings.className = 'qt-menu-item';
    liSettings.innerHTML = '<a href="#">qtSettings</a>';

    liSettings.onclick = function(e){
      e.preventDefault();

      var settings = document.querySelector('.qt-settings');
      if(settings.style.display == 'block'){
        settings.style.display = 'none';
      } else {
        settings.style.display = 'block';
      }
    };

    ul.appendChild(liSettings);

    div.appendChild(ul);

    li.appendChild(div);

    nav.insertBefore(li, profile);
  };

  window.qtSettings = function(){
    window.addStyle(
      '.qt-settings {' +
      '  display: none;' +
      '  position: fixed;' +
      '  top: 0;' +
      '  height: 50%;' +
      '  z-index: 10000;' +
      '}' +

      '.qt-settings > .panel-heading {' +
      '  cursor: move;' +
      '}' +

      '.qt-settings > .panel-body {' +
      '  height: 100%;' +
      '}' +

      '.qt-settings .qt-settings-body {' +
      '  height: 90%;' +
      '  overflow: auto;' +
      '}');

    var div = document.createElement('div');
    div.id = 'qt-settings';
    div.className = 'qt qt-settings panel panel-info';
    div.style.top = localStorage['qt-settings-x'] + 'px';
    div.style.left = localStorage['qt-settings-y'] + 'px';

    var head = document.createElement('div');
    head.className = 'panel-heading';
    head.innerHTML = 'qtSettings';

    var a = document.createElement('a');
    a.href = '#';
    a.style.float = 'right';
    a.innerHTML = 'X';
    
    head.appendChild(a);

    a.onclick = function(e){
      e.preventDefault();

      this.parentNode.parentNode.style.display = 'none';
      window.qtInit();
    };

    var body = document.createElement('div');
    body.className = 'panel-body';

    var html = 
    '  <div class="qt-settings-body">' +
    '  <label><input type="checkbox" onchange="localStorage[\'qt-hide-nav\']=this.checked;" ' + (localStorage['qt-hide-nav'] == 'true' ? 'checked' : '') + '/> Disable nav menu</label><br/>' +
    '  <label><input type="checkbox" onchange="localStorage[\'qt-hide-top\']=this.checked;" ' + (localStorage['qt-hide-top'] == 'true' ? 'checked' : '') + '/> Disable top menu</label><br/>' +
    '  <label><input type="checkbox" onchange="localStorage[\'qt-hide-market\']=this.checked;" ' + (localStorage['qt-hide-market'] == 'true' ? 'checked' : '') + '/> Hide Market</label><br/>';

    for(var i = 0;i < cities.length;i++){
      html += '<label><input type="checkbox" onchange="localStorage[\'qt-hide-city'+i+'\']=this.checked;" ' + (localStorage['qt-hide-city'+i] == 'true' ? 'checked' : '') + ' /> Hide ' + cities[i] + '</label><br/>';
    }

    for(i = 0;i < professions.length;i++){
      html += '<label><input type="checkbox" onchange="localStorage[\'qt-hide-profession'+i+'\']=this.checked;" ' + (localStorage['qt-hide-profession'+i] == 'true' ? 'checked' : '') + '/> Hide ' + professions[i] + '</label><br/>';
    }

    html += '<label><input type="checkbox" onchange="localStorage[\'qt-hide-gathering\']=this.checked;" ' + (localStorage['qt-hide-gathering'] == 'true' ? 'checked' : '') + '/> Hide Gathering</label><br/>';
    for(i = 0;i < gatheringGrounds.length;i++){
      html += '<label><input type="checkbox" onchange="localStorage[\'qt-hide-gathering'+i+'\']=this.checked;" ' + (localStorage['qt-hide-gathering'+i] == 'true' ? 'checked' : '') + '/> Hide ' + gatheringGrounds[i] + '</label><br/>';
    }

    html += '<label><input type="checkbox" onchange="localStorage[\'qt-hide-combat\']=this.checked;" ' + (localStorage['qt-hide-combat'] == 'true' ? 'checked' : '') + '/> Hide Combat</label><br/>';
    for(i = 0;i < combatZones.length;i++){
      html += '<label><input type="checkbox" onchange="localStorage[\'qt-hide-combat'+i+'\']=this.checked;" ' + (localStorage['qt-hide-combat'+i] == 'true' ? 'checked' : '') + '/> Hide ' + combatZones[i] + '</label><br/>';
    }

    html += '</div>';
    body.innerHTML = html;

    div.appendChild(head);
    div.appendChild(body);
    document.body.appendChild(div);

    head.onmousedown = function(e){
      this.x = e.clientX;
      this.y = e.clientY;
      window.qtMove = this;
    };
  };

  var qtAdditionTicksArray = [
    [360, 396, 475, 523, 575, 661],
    [720, 792, 871, 958, 1053, 1210]
  ];
  window.qtAdditionTicks = function(t,l,a){
    return (qtAdditionTicksArray[t > 0 ? 1 : 0][l-1] || 0) * a;
  };

  window.qtActivationPrice = function(b,f,a,l,x){
    if(l >= 3) {
      b = b * 1.2;
    }

    for(var i=5;i <= l;i++){
      if(i%2 == 1) {
        b = b * f;
      }
    }

    var c = ((a>1) ? b : 0);
    var s = c;
    for(i=2;i < a;i++){
      c = c*1.25;
      s += c;
    }
    if(x) return Math.round(c);
    return Math.round(s);
  };

  window.qtMasteryPrice = function(a,l,x){
    return window.qtActivationPrice(100000,1.125,a,l,x);
  };

  window.qtSuccessPrice = function(a,l,x){
    return window.qtActivationPrice(30000,(1+1/6),a,l,x);
  };

  window.qtDifficultyPrice = function(a,l,x){
    return window.qtActivationPrice(50000,1.3,a,l,x);
  };

  window.qtEssencePrice = function(a,l,x){
    var c = ((a>1) ? 10000 : 0);
    var s = c;
    for(var i=2;i < a;i++){
      c = c*1.25;
      s += c;
    }
    if(x) return Math.round(c);
    return Math.round(s);
  };

  window.qtCalcAdditions = function(){
    var f = [window.qtMasteryPrice, window.qtSuccessPrice, window.qtDifficultyPrice, window.qtEssencePrice];
    var cl = 0;
    var ce = 0;
    var i=0;
    for(;i<100;i++){
      if(localStorage['qt-addition-id'+i]){
        var input = document.getElementById('qt-calculator-activations'+i);
        if(!input){
          continue;
        }
        var t = localStorage['qt-addition-type'+i];
        var l = localStorage['qt-addition-level'+i]||1;
        if(t < 3){
          cl += f[t](input.value,l);
        } else {
          ce += f[t](input.value,l);
        }

        var inputTicks = document.getElementById('qt-calculator-ticks'+i);
        if(!inputTicks){
          continue;
        }
        inputTicks.value = window.qtAdditionTicks(t, l, input.value);
      } else {
        break;
      }
    }

    var r = /(\d)(?=(\d{3})+$)/gm;
    document.getElementById('qt-calculator-daily-leol').value = cl.toFixed().replace(r, '$1,');
    document.getElementById('qt-calculator-weekly-leol').value = (cl*7).toFixed().replace(r, '$1,');
    document.getElementById('qt-calculator-daily-essence').value = ce.toFixed().replace(r, '$1,');
    document.getElementById('qt-calculator-weekly-essence').value = (ce*7).toFixed().replace(r, '$1,');
  };

  window.qtCalcAdditionsSave = function(){
    for(var i=0;i<100;i++){
      if(localStorage['qt-addition-id'+i]){
        var input = document.getElementById('qt-calculator-activations'+i);
        if(!input){
          continue;
        }
        localStorage['qt-addition-goal'+i] = input.value;
        var td = document.querySelector('#qt-addition-goal'+i);
        if(td) td.innerHTML = input.value;
      }
    }
  };

  window.qtCalculator = function(){
    window.addStyle(
      '.qt-calculator {' +
      '  display: none;' +
      '  position: fixed;' +
      '  top: 0;' +
      '  z-index: 10000;' +
      '}' +

      '.qt-calculator > .panel-heading {' +
      '  cursor: move;' +
      '}' +

      '.qt-calculator > .panel-body {' +
      '  height: 100%;' +
      '}' +

      '.qt-calculator > .panel-body > select {' +
      '  width: 100%;' +
      '}' +

      '.qt-calculator td > input {' +
      '  width: 100%;' +
      '}' +

      '.qt-calculator .qt-calculator-body {' +
      '  height: 90%;' +
      '  overflow: auto;' +
      '}');

    var div = document.createElement('div');
    div.id = 'qt-calculator';
    div.className = 'qt qt-calculator panel panel-info';
    div.style.top = localStorage['qt-calculator-x'] + 'px';
    div.style.left = localStorage['qt-calculator-y'] + 'px';

    var head = document.createElement('div');
    head.className = 'panel-heading';
    head.innerHTML = 'qtCalculator';

    var a = document.createElement('a');
    a.href = '#';
    a.style.float = 'right';
    a.innerHTML = 'X';
    
    head.appendChild(a);

    a.onclick = function(e){
      e.preventDefault();

      this.parentNode.parentNode.style.display = 'none';
    };

    var body = document.createElement('div');
    body.className = 'panel-body';

    var html = 
    '<select>' +
    '  <option value="qt-calculator-additions">Additions</option>' +
    '</select>' +
    '<div>' +
    '  <div id="qt-calculator-additions">' +
    '    <table>';

    for(var i=0;i<100;i++){
      if(localStorage['qt-addition-id'+i]){
        html += 
          '      <tr>' +
          '        <td>' + localStorage['qt-addition-name'+i] + ' (Lv.' + localStorage['qt-addition-level'+i] + ((localStorage['qt-addition-type'+i]) ? ' ' + ['Mastery', 'Success', 'Difficulty', 'Combat'][localStorage['qt-addition-type'+i]] : '') + '):</td>' +
          '        <td><input id="qt-calculator-ticks' + i + '" type="text" disabled /></td>' +
          '        <td><input id="qt-calculator-activations' + i + '" type="number" min="1" onchange="window.qtCalcAdditions()" value="' + (localStorage['qt-addition-goal'+i]||1) + '" /><td/>' +
          '      </tr>';
      }
    }

    html += 
    '      <tr>' +
    '        <td>Daily Leol:</td>' +
    '        <td colspan="2"><input id="qt-calculator-daily-leol" type="text" disabled /><td/>' +
    '      </tr>' +
    '      <tr>' +
    '        <td>Weekly Leol:</td>' +
    '        <td colspan="2"><input id="qt-calculator-weekly-leol" type="text" disabled />' +
    '      </tr>' +
    '      <tr>' +
    '        <td>Daily Essence:</td>' +
    '        <td colspan="2"><input id="qt-calculator-daily-essence" type="text" disabled /><td/>' +
    '      </tr>' +
    '      <tr>' +
    '        <td>Weekly Essence:</td>' +
    '        <td colspan="2"><input id="qt-calculator-weekly-essence" type="text" disabled />' +
    '      </tr>' +
    '      <tr>' +
    '        <td colspan="3"><input type="button" value="Save" onclick="window.qtCalcAdditionsSave()" /></td>'
    '      </tr>' +
    '    </table>' +
    '  </div>' +
    '</div>';

    body.innerHTML = html;
    div.appendChild(head);
    div.appendChild(body);
    document.body.appendChild(div);

    head.onmousedown = function(e){
      this.x = e.clientX;
      this.y = e.clientY;
      window.qtMove = this;
    };

    window.qtCalcAdditions();
  };

  window.qtActivateBonus = function(i){
    document.querySelector('.navbar-right > .qt-nav > a').className = '';
    window.qtRequest("POST", "/user/activate_bonus?ua_id=" + i, function(){
      window.qtCheckAdditions();
    });
  };

  window.qtAdditions = function(){
    window.addStyle(
      '.qt-additions {' +
      '  display: none;' +
      '  position: fixed;' +
      '  top: 0;' +
      '  z-index: 10000;' +
      '}' +

      '.qt-additions > .panel-heading {' +
      '  cursor: move;' +
      '}' +

      '.qt-additions > .panel-body {' +
      '  height: 100%;' +
      '}' +

      '.qt-additions > .panel-body > select {' +
      '  width: 100%;' +
      '}' +

      '.qt-additions td > input {' +
      '  width: 100%;' +
      '}' +

      '.qt-additions .qt-additions-body {' +
      '  height: 90%;' +
      '  overflow: auto;' +
      '}' +

      '.qt-additions table td:not(:first-child) {' +
      '  text-align: center;' +
      '}'
      );

    var div = document.createElement('div');
    div.id = 'qt-additions';
    div.className = 'qt qt-additions panel panel-info';
    div.style.top = localStorage['qt-additions-x'] + 'px';
    div.style.left = localStorage['qt-additions-y'] + 'px';

    var head = document.createElement('div');
    head.className = 'panel-heading';
    head.innerHTML = 'qtAdditions';

    var a = document.createElement('a');
    a.href = '#';
    a.style.float = 'right';
    a.innerHTML = 'X';
    
    head.appendChild(a);

    a.onclick = function(e){
      e.preventDefault();

      this.parentNode.parentNode.style.display = 'none';
    };

    var body = document.createElement('div');
    body.className = 'panel-body';

    var html = 
      '<table>' +
      '  <tr>' +
      '    <td colspan="4">Automatic</td>' +
      '    <td><input type="checkbox"  onchange="localStorage[\'qt-additions-automatic\']=this.checked;qtInit();" ' + (localStorage['qt-additions-automatic'] == 'true' ? 'checked' : '') + '/></td>' +
      '  </tr>';

    for(var i=0;i<100;i++){
      if(localStorage['qt-addition-id'+i]){
        html +=
          '  <tr>' +
          '    <td>' + localStorage['qt-addition-name'+i] + ' (Lv. ' + localStorage['qt-addition-level'+i] + ((localStorage['qt-addition-type'+i]) ? ' ' + ['Mastery', 'Success', 'Difficulty', 'Combat'][localStorage['qt-addition-type'+i]] : '') + '):</td>' +
          '    <td id="qt-addition-used' + i + '">' + (localStorage['qt-addition-used'+i]||0) + '</td>' +
          '    <td>/</td>' +
          '    <td id="qt-addition-goal' + i + '">' + (localStorage['qt-addition-goal'+i]||0) + '</td>' +
          '    <td><input type="button" value="+" onclick="window.qtActivateBonus(' + localStorage['qt-addition-id'+i] + ')" class="btn btn-sm btn-primary" /></td>' +
          '  </tr>';

      }
    }
    
    html += '</table>';

    body.innerHTML = html;

    div.appendChild(head);
    div.appendChild(body);
    document.body.appendChild(div);

    head.onmousedown = function(e){
      this.x = e.clientX;
      this.y = e.clientY;
      window.qtMove = this;
    };

    window.qtCalcAdditions();
  };

  window.qtSetCurrentTabActive = function(){
    localStorage.setItem('qt-uuid', window.qtUUID + ';' + new Date().getTime());
  };

  window.qtCheckingAdditions = [];
  window.qtCheckAdditions = function(){
    var activeUUID = localStorage.getItem('qt-uuid');

    if(activeUUID != null){
      activeUUID = activeUUID.split(';');
      if(activeUUID[0] != window.qtUUID){
        if((new Date().getTime() - Number.parseInt(activeUUID[1])) > 120000){
          window.qtSetCurrentTabActive();
        }
        return;
      }
    } else {
      window.qtSetCurrentTabActive();
      return;
    }

    window.qtSetCurrentTabActive();

    for(var i = 0;i < 100;i++){
      var a = localStorage['qt-addition-id'+i];
      if(a){
        if(window.qtCheckingAdditions[i] == true){
          continue;
        }
        
        window.qtCheckingAdditions[i] = true;

        window.qtRequest("POST", "/user/open_addition?addition_id=" + a, function(i){
          if(this.readyState != 4){
            return;
          }

          if(this.status != 200){
            window.qtCheckingAdditions[i] = false;
            return;
          }

          var f = [window.qtMasteryPrice, window.qtSuccessPrice, window.qtDifficultyPrice, window.qtEssencePrice][localStorage['qt-addition-type'+i]];
          if(!f){
            window.qtCheckAdditions[i] = false;
            return;
          }
          
          var r = /<li>(Free)|<strong>([\d,]+)/gm;
          var m = r.exec(this.responseText);
          if(!m || m.length <= 0) return;

          var c = ~~((m[1]||m[2]).replace(/,/g,''));

          var l = localStorage['qt-addition-level'+i]||1;
          var x = 0;
          while(true){
            if(f(x+1,l,true) >= c) break;
            x++;
          }

          localStorage['qt-addition-used'+i] = x;
          var td = document.querySelector('#qt-addition-used'+i);
          if(td) td.innerHTML = x;

          var z = localStorage['qt-addition-goal'+i];

          window.qtCheckingAdditions[i] = false;

          if(x < z){
            document.querySelector('.navbar-right > .qt-nav > a').className = 'qt-notify';
            if(localStorage['qt-additions-automatic'] == 'true'){
              window.qtActivateBonus(localStorage['qt-addition-id'+i]);
            }
          }
        },
        function(i){
          window.qtCheckingAdditions[i] = false;
        },i);
      }
    }
  };

  document.getElementById('skill_details').addEventListener('DOMSubtreeModified', function(e){
    if(!e || !e.srcElement || e.srcElement.id != 'skill_details'){
      return;
    }

    if(e.srcElement.qt === true){
      return;
    }

    e.srcElement.qt = true;
    e.srcElement.innerHTML = e.srcElement.innerHTML.replace(/(Mastery\s+Progress\:\s+(\d+)\/)([^\d\s]+)/, '$1∞');
    e.srcElement.qt = false;
  });

  window.qtInit = function(){
    var qts = document.querySelectorAll('.qt');
    for(var i = 0;i < qts.length;i++){
      qts[i].remove();
    }
    window.qtResize();
    window.qtMenu();
    window.qtNavMenu();
    window.qtSettingsMenu();
    window.qtSettings();
    window.qtCalculator();
    window.qtAdditions();
    window.qtCheckAdditions();
  };

  window.qtResize = function(){
    document.querySelectorAll('.container-fluid')[1].style['padding-top'] = (parseInt(getComputedStyle(document.querySelector('.navbar-fixed-top'))['height'])+5) + 'px';
  };

  window.onresize = window.qtResize;
  window.onload = window.qtResize;

  document.onmousemove = function(e){
    if(!window.qtMove) return;

    var nt = ((parseInt(window.qtMove.parentNode.style.top)||0)-(window.qtMove.y-e.clientY));
    var nl = ((parseInt(window.qtMove.parentNode.style.left)||0)-(window.qtMove.x-e.clientX));
    
    window.qtMove.parentNode.style.top = (nt <= 0 ? 0 : (nt > (window.innerHeight-window.qtMove.offsetHeight)) ? (window.innerHeight-window.qtMove.offsetHeight) : nt) + 'px';
    window.qtMove.parentNode.style.left = (nl <= 0 ? 0 : (nl > (window.innerWidth-window.qtMove.offsetWidth)) ? (window.innerWidth-window.qtMove.offsetWidth) : nl) + 'px';

    window.qtMove.x = e.clientX;
    window.qtMove.y = e.clientY;

    localStorage[window.qtMove.parentNode.id + '-x'] = nt;
    localStorage[window.qtMove.parentNode.id + '-y'] = nl;
  };

  document.onmouseup = function(e){
    window.qtMove = false;
  };

  function wait(s,f){
    var l = document.querySelectorAll(s);
    var b = false;
    if(l && l.length > 0){
      for(var i = 0;i < l.length;i++){
        if(!l[i].found){
          b = true;
          l[i].found = true;
        }
      }

      if(b) f();
    }

    setTimeout(function(){wait(s,f);},300);
  }

  window.qtUUID = window.uuidv4();
  window.qtLoad();
  window.qtCheckAdditions();
  setInterval(window.qtCheckAdditions, 5000);
  wait('.main-page', window.qtInit);
})();